The final scripts will be merged into the Kimetrica Luigi pipeline.

1. Calculations and Outputs, run_calc_output.py will run all of the following:

(Calculations by Section)
-HH_calc.py
-Impact_calc.py
-WASH_calc.py
-Education_calc.py
-Health_calc.py
-Nutrition_calc.py
-CP_calc.py


(All Table Outputs)
-tableoutput_all.py

(Significant Table Outputs)
-tableoutput_signf.py

2. Regression Exploration
-regression_expl.py
