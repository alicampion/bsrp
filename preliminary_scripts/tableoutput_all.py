import pickle
import table_methods as tm

tablefiles = {}

for key, value in tablefiles:
    f =  open(value, 'rb')
    tableinfo = pickle.load(f)
    
    writer = pd.ExcelWriter(key, engine='xlsxwriter')

    for item in range(6,55):
        if table_info[item][2] == 'percent':
                dftemp = p_rc_table(df,table_info[item][1])
                dftemp.to_excel(writer, sheet_name=item)
            
                dftemp = p_rh_table(df,table_info[item][1])
                dftemp.to_excel(writer, sheet_name=item, startcol=6)
            
        if table_info[item][2] == 'distribution':
                dftemp = d_rc_table(df,table_info[item][1])
                dftemp.to_excel(writer, sheet_name=item)
     
                dftemp = d_rh_table(df,table_info[item][1])
                dftemp.to_excel(writer, sheet_name=item, , startcol=6)
            