
#   Utilization Regressions (15-20)
#      WASH(4), Education(1), Health and Nutrition(7)

wash1 = {"HHQQ521":["HHQQ521","HHQQ523","HHQQ523","HHQQ541","HHQQ533","HHQQ512c","HHQQ711"]}
education1 = {}
health1 = {}
wash2 = {"HHQQ761c":["HHQQ1001","HHQQ556","HHQQ571","HHQQ581","HHQQ591","HHQQ534","HHQQ762b","HHQQ762","HHQQ219"]}
education2 = {}
healthnutrition2 = {}

utilisation = [wash1,education1,health1]
welfare = [wash2,education2,health2]


import statsmodels.api as sm
import itertools 


def runlog():
    x = sm.add_constant(data.exog, prepend=False)
    y = data.endog
    res1 = sm.Logit(y, x).fit()
    print res1.summary()
    #cross val
    return()


def rankfor(s,Y):
    combolist = combos(s,Y)
    for c in combos:
        X = c
        runlog(X,Y)
        collcheck()


def combos(s,Y):
    X = s[Y]
    combolist = []
    for i in range(0, len(X)+1):
          for subset in itertools.combinations(X, i):
                combolist.append(subset)
    return(combolist)
