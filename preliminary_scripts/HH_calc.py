import table_methods as tm
import pandas as pd
import pickle

df = pd.read_csv('cleaned_data.csv',encoding='latin-1' )
df = tm.disaggregate(df)

table_info = {}


#only run first time, second time: pull from csv.

#df_hh = tm.households_only(df)

#second time:

#df_hh = pd.read_csv('df_hh.csv',encoding='latin-1')

#6
#Number of households sampled in each location (region, for urban: kebele; for rural: kebele, village; for refugee: subcamp, zone)

#7
#Number of individuals in the sampled households in each location (as above)

#8
#Percent of all households with at least one child of primary school age (7-14) in the household
child = 'Q214a'
df['Q214a_child714_hh'] = 0

for index, row in df.itterrows():
    currhh = row[hh]
    length = len(df.loc[(df[hh] == currhh) & (7 <= df[child] <= 14)])
    if length > 0 :
        df.loc[index,'Q214a_child714_hh'] = 1
        
table_info[8] = ('df_hh','Q214a_child714_hh','percent')

#9
#Percent of all individuals in the sampled households that are children of primary school age (7-14)
df['Q214a_child714_ind'] = 0

for index, row in df.itterrows():
    if (7 <= row[child] <=14]):
        df.loc[index,'Q214a_child714_ind'] = 1

table_info[9] = ('df','Q214a_child714_ind','percent')

#10
#Percent of all households with at least one child under 5 in the household
df['Q214a_child5_hh'] = 0

for index, row in df.itterrows():
    currhh = row[hh]
    length = len(df.loc[(df[hh] == currhh) & (df[child] < 5)])
    if length > 0 :
        df.loc[index,'Q214a_child5_hh'] = 1
        
table_info[10] = ('df_hh','Q214a_child5_hh','percent')

#11
#Percent of all individuals in the sampled households that are children under 5

df['Q214a_child5_ind'] = 0

for index, row in df.itterrows():
    if (row[child] < 5]):
        df.loc[index,'Q214a_child5_ind'] = 1

table_info[11] = ('df','Q214a_child5_ind','percent')

#12
#Percent of all households with at least one pregnant woman in the household

df['Q751a_preg_hh'] = 0

for index, row in df.itterrows():
    currhh = row[hh]
    length = len(df.loc[(df[hh] == currhh) & (df['Q751a'] == 1)])
    if length > 0 :
        df.loc[index,'Q751a_preg_hh'] = 1
        
table_info[12] = ('df_hh','Q751a_preg_hh','percent')

#13
#Percent of all individuals in the sampled households that are pregnant

table_info[13] = ('df','Q751a','percent')

#14 DONT HAVE THIS
#Percent of all households with at least one lactating woman in the household

#15 DONT HAVE THIS
#Percent of all individuals in the sampled households that are lactating

#16
#Percent of all households where the main caregiver was interviewed, was not interviewed, and where there are no children under age 18 in the HH

table_info[16] = ('df_hh','Q132a','percent')

#17
#Of the households where the main caregiver was not interviewed but where there were no children under 18 in the household, reasons why the main caregiver was not interviewed

#need custom denominator

table_info[17] = ('df_hh','Q132b','percent')

#18
#Percent of all households where the respondent is the HoH

table_info[18] = ('df_hh','Q132c','percent')

#19
#Of the households where the main caregiver was not interviewed, percent of households where the respondent is the HoH

#need custom denominator
table_info[19] = ('df_hh','Q132c','percent')

#20
#Percent of households where the respondent was female

table_info[20] = ('df_hh','Q134','percent')

#21
#Pecent of refugees living in host community, and percent of host community living in refugee camp

#need custom denominators

table_info[21] = ('df','Q136','percent')

# Household Demographics

#22
#Country of origin
#Ethnicity
#Language
#How much time you've spent in this location
#Religion

table_info[22.1] = ('df_hh','Q137a','percent','nested')
table_info[22.2] = ('df_hh','Q137b','percent','nested')
table_info[22.3] = ('df_hh','Q137d','percent','nested')
table_info[22.4] = ('df_hh','Q137e','percent','nested')
table_info[22.5] = ('df_hh','Q138','percent','nested')

#23
#Pastoralist

table_info[23] = ('df_hh','Q137d','percent')

#24
#For the households that live in the refugee camp, or of the households that are refugees?: Distribution of year of arrival

table_info[24] = ('df_hh','Q139b','distribution')

#25
#For the households that live in the refugee camp, or of the households that are refugees?: Percent of HHs that have one ration card, 2 ration cards, 3 ration cards, or more.

table_info[25] = ('df_hh','Q142','percent')

#26
#Percent of all households with at least one HH member who has completed tertiary, completed TVET or other technical training, completed secondary school, completed primary school

table_info[26] = ('df_hh','Q141','percent')

#27
#Percent of households size 1, size 2, size 3-5, size 5-8, size 9+ (or other logical grouping based on distribution of data)

table_info[27] = ('df_hh','Q141a','percent')

#28

table_info[28] = ('df_hh','Q141a','distribution')

#29
#Percent of all households with household members who are not related by blood. Based on findings we might want some others stats for this.

table_info[29] = ('df_hh','Q141b','percent')

# Household Composition

#30
#Minimum, median, average, maximum age dependency ratio (World Bank definition: Number of dependents age 0-14 and 65+) per working age member (age 15-64)

#ratio calculations

table_info[30] = ('df_hh','Q214a','percent')

#31
#Percent of households with earning potential (at least one adult that does not need to care for children or disabled/elderly members) 

table_info[31] = ('df_hh','Q214a','percent')


# Care/mother
# Wealth assets

#33
#For each of the wealth indicators (mobile phone, radio, television, table, bicycle, solar panel, fishing boat, fishing net -- one table for each): Percent of all households that have at least one

table_info[33] = ('df_hh','Q311','percent')

#34
#Minimum, median, mean, and maximum: Crowding index: beds per capita 

#custom denominator

table_info[34] = ('df_hh','Q311f','percent')

#35
#Minimum, median, mean, and maximum: Crowding index: sleeping rooms per capita

table_info[35] = ('df_hh','Q312','percent')

#36
#Percent of households that have electricity

table_info[36] = ('df_hh','Q313','percent')

#37
#Percent of all households with electricity from each source

table_info[37] = ('df_hh','Q313b','percent')

#38

#39
#Percent of households owning at least one animal

table_info[39] = ('df_hh','Q314','percent')

# Income

#40 
#Percent of households with each main livelihood

table_info[40] = ('df_hh','Q321','percent')

#41
#For each: Business ownership, formal and informal employment, earned cash income (one table for each): Percent of households with at least one member who xxxxx.

table_info[41] = ('df_hh','Q321','percent')


#42
#Percent of households that have received remittances in the last 30 days

table_info[42] = ('df_hh','Q323','percent')

#43
#Percent of households that have received food assistance in the last 30 days

table_info[43] = ('df_hh','Q324a','percent')

#44
#Of the households that have received food assistance in the last 30 days, percent that sold any of it

table_info[44] = ('df_hh','Q324b','percent')

#45
#Percent of households that have received cash or voucher assistance in the last 30 days

table_info[45] = ('df_hh','Q324c','percent')

#46
#Percent of households that have received NFI assistance in the last 30 days

table_info[46] = ('df_hh','Q324d','percent')

#47
#Percent of households with money owed to them

table_info[47] = ('df_hh','Q325a','percent')

#48
#Percent of households with debts

table_info[48] = ('df_hh','Q325b','percent')

#49
#Percent of households with savings - looks to be same as above

table_info[49] = ('df_hh','Q325b','percent')


# Food access

#50
#Percent of households that farm

table_info[50] = ('df_hh','Q411a','percent')

#51
#Percent of households with a kitchen garden

table_info[51] = ('df_hh','Q411b','percent')

#52
#Percent of households whose main source of food is each source

table_info[52] = ('df_hh','Q412','percent')

#53
#Minimum, median, mean, and maximum: distance to market

table_info[53] = ('df_hh','Q413','percent')

#54
#Market purchases


file = open('hh.pkl', 'w')
pickle.dump(table_info,file)