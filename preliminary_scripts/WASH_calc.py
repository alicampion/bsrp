import table_methods as tm
import pandas as pd
import pickle

df = pd.read_csv('cleaned_data.csv',encoding='latin-1' )

table_info = {}


# Outputs

#1
#Percent of households with access to an improved water supply within 30 minutes' walk

table_info[1] = ('df_hh','Q511','percent')

#2
#Average round-trip water collection time (main source, improved source)

table_info[2] = ('df_hh','Q512b','percent')

#3
#Percent of households who did not have water from their main source for a day or longer in the past 2 weeks

table_info[3] = ('df_hh','Q513','percent')

#4
#Percent of households paying for water

table_info[4] = ('df_hh','Q541a','percent')

#5
#Average amount households are willing to pay for water

table_info[4] = ('df_hh','Q543','percent')

#5
#Average amount households are willing to pay for water

table_info[5] = ('df_hh','Q543','percent')

#6

#7

#8
#Percent of households that have access to basic sanitation facilities (an improved latrine that is not shared) (age of latrine)

table_info[8] = ('df_hh','Q551','percent')

#9
#Percent of households with access to a clean, private and structurally safe toilet facility

table_info[9] = ('df_hh','Q901','percent')

#10
#Percent of households who feel safe when accessing the toilet facility

table_info[10] = ('df_hh','Q555','percent')

#11 
#Percent of households that would be willing to pay for solid waste disposal services
#Of these, average amount they would be willing to pay for this

table_info[11.1] = ('df_hh','Q583a','percent')
table_info[11.2] = ('df_hh','Q583b','percent')

#12
#13
#14

#15
#Percent of households who have access to handwashing facilities (near or inside latrine)

table_info[15] = ('df_hh','Q561','percent')

#16

#17
#Percent of households receiving a WASH message in the last three months through any medium (topic, medium)

table_info[17] = ('df_hh','Q711','percent')

#18

#Outcomes

#19
#Percent of households that use an improved drinking water source

table_info[19] = ('df_hh','Q511','percent')

#20
#Percent of households that treat their drinking water using an appropriate method

table_info[20] = ('df_hh','Q533','percent')

#21
#Of households that store water in containers, Percent that use safe water storage techniques

table_info[21] = ('df_hh','Q534','percent')

#22
#Average amount of water consumed per capita per day (yesterday, usually)

table_info[22] = ('df_hh','Q531','percent')

#23

#24
#Percent of households who do not practice open defecation

table_info[24] = ('df_hh','Q551','percent')

#25
#Percent of households that report using a safe solid waste collection and disposal service

table_info[25] = ('df_hh','Q581','percent')

#26
#Percent of households that are free from indiscriminately disposed solid waste

table_info[26] = ('df_hh','Q582','percent')

#27

#28
#Percent of households with children under 5 reporting the hygienic disposal of the stools of children under 5

table_info[28] = ('df_hh','Q556','percent')

#29
#Percent of respondents that wash their hands with soap or ash at all four critical times: before eating, before cooking/preparing food, after defecating/urinating, after cleaning a child who has defecated

table_info[29] = ('df_hh','Q571','percent')


# Welfare

#30
#Percentage of HHs with a < 5 year old reporting diarrhoea in the past 2 weeks

table_info[29] = ('df_hh','Q781c','percent')

file = open('wash.pkl', 'w')
pickle.dump(table_info,file)

