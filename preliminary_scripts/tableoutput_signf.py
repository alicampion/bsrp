import pickle
import table_methods as tm

tablefiles = {}

for key, value in tablefiles:
    f =  open(value, 'rb')
    tableinfo = pickle.load(f)
    
    writer = pd.ExcelWriter(key, engine='xlsxwriter')
    
    for item in range(6,55):
    
        if table_info[item][1] == 'percent':
            test = chi_square(df,table_info[item][0])
        
            if test['community_p'] < .05:       
                dftemp = p_rc_table(df,table_info[item][0])
                dftemp.to_excel(writer, sheet_name=item)
        
            if test['hoh_p'] < .05:        
                dftemp = p_rh_table(df,table_info[item][0])
                dftemp.to_excel(writer, sheet_name=item, startcol=6)
            
        if table_info[item][1] == 'distribution':
            ttest = t_test(df,table_info[item][0])
            anova = anova(df,table_info[item][0])
        
            if anova['community_p'] < .05:        
                dftemp = d_rc_table(df,table_info[item][0])
                dftemp.to_excel(writer, sheet_name=item)
        
            if test['hoh_p'] < .05:        
                dftemp = d_rh_table(df,table_info[item][0])
                dftemp.to_excel(writer, sheet_name=item, , startcol=6)