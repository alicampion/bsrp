import table_methods as tm
import pandas as pd
import pickle

df = pd.read_csv('cleaned_data.csv',encoding='latin-1' )

table_info = {}

# Refugee/host interaction

#1
#Percentage of households from host and refugee communities reporting their acceptance for the other community (refugees/host communities) using their local basic social services, BY SECTOR

table_info[1] = ('df_hh','Q544a','percent')

#2
#Percentage of households from host and refugee communities reporting their acceptance for the other community (refugees/host communities) using their local basic social services, FOR ALL SECTORS

table_info[2] = ('df_hh','Q544a','percent')

#3
#Percent of households who report that it is acceptable for children/people of BOTH communities to use the services of the other, BY SECTOR
table_info[3] = ('df_hh','Q544a','percent')

#4
#Percentage of households from host communities and refugees communities reporting using basic services in the other community, BY SECTOR
table_info[4] = ('df_hh','','percent')

#5
#Percent of all households whose main source of drinking water is in the other community
table_info[5] = ('df_hh','Q512a','percent')

#6
#Percent of households with a child attending primary school that sends their child to school in the other community
table_info[6] = ('df_hh','Q628a','percent')

#7
#(Of those households that use a health facility) Percent of households who [visit a health facility / whose MAIN health facility] is in the other community
table_info[7] = ('df_hh','Q722','percent')

#8
#Percent of households that knows someone who has accessed a CP service in the other community
table_info[8] = ('df_hh','Q814','percent')


# Health, nutrition, WASH

#9
#Percent of HHs with a < 5 year old reporting diarrhoea in the past 2 weeks
table_info[9] = ('df_hh','Q762c_2','percent')

#10
#Percent of all children <5 with diarrhoea in the past 2 weeks
table_info[10] = ('df_hh','Q762c_2','percent')


#11
#Percent of children under 5 years with stunting (height/length-for-age is <-2 standard deviations below the WHO Child Growth Standards median) (HHQ Q762b)
table_info[11] = ('df_hh','Q762a_2','percent')

# Health

#12
#Percent of households reporting an improvement in the quality of services provided at the health centres in the last year

table_info[12] = ('df_hh','Q724i','percent')

#13
#Percent of households reporting an improvement in the quality of services provided at at least one health centre they visit in the last year

table_info[13] = ('df_hh','Q724i','percent')

#14
#Percent of households reporting an improvement in the quality of services provided at each health centre in the last one year

table_info[14] = ('df_hh','Q724i','percent')

#Education

#15
#Primary school completion rate, by cycle (using grades 1-4 as first cycle with GoE definition of official graduating age of 10 years and grades 5-8 as second cycle with GOE definition of official graduating age of 14 years as per http://info.moe.gov.et/emdocs/esaa01.pdf)
table_info[15] = ('df_hh','Q625','percent')

file = open('impact.pkl', 'w')
pickle.dump(table_info,file)