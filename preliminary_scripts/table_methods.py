import pandas as pd
import numpy as np


def disaggregate(df):
   
   df['n'] = 1
   disaggs = {'Region','Community','Gender','Disability'}



   disability = 'Q218a' #need household identifier

   #1 Modify with new
   region = 's1q121' 
    
   regdict = {1:'Afar',2:'Benishangul-Gumuz',3:'Gambella',4:'Somali',5:'Tigray'}
    
   for index, row in df.iterrows():
       df.loc[index, 'Region'] = regdict[row[region]]
   
   #2 Modify with new 
   host_refug = 's1q122'
   urban_rural = 's1q123'

   df['Community'] = df[host_refug]
   df.loc[df.Community ==1,'Community'] = 'Camp'
   df.loc[(df.Community ==2) & (df[urban_rural] == 1),'Community'] = 'Rural'
   df.loc[(df.Community ==2) & (df[urban_rural] == 2),'Community'] = 'Urban'
   #3 Uncomment this block with data
    
  # gender = 'Q213'
   #head = 'Q212b'

   #df['Gender'] = 'Not Head of Household'
   #df.loc[(df[head] ==1) & (df[gender] == 1),'Gender'] = 'Male'
   #df.loc[(df[head] ==1) & (df[gender] == 2),'Gender'] = 'Female'
   return(df)


def hh_disability(df):
   disability = 'Q218a'
   hh = 'Q123'
   df['Disability'] = 0

   for index, row in df.itterrows():
        currhh = row[hh]
        length = len(df.loc[(df[hh] == currhh) & (df[disability] ==1)])
        if length > 0 :
            df.loc[index,'Disability'] = 1
   
   return(df)

def household_only(df):
    
    #need to update
    hh = 's1q113'
    df_hh = df.groupby(hh).first()
    return(df_hh)

def chi_square_rc(df,indicator):
    
    # create pivots for disaggs
    df2 = df
    df2.loc[df[indicator] == -777, indicator] = 0
    dftemp = pd.pivot_table(df2,index='Region',
                                    columns = 'Community',
                                    values=indicator,
                                    aggfunc = np.sum,
                                    margins=True)
    
    df2 = df
    df2.loc[df[indicator] == -777, indicator] = 0
    dftemp = pd.pivot_table(df2,index='Region',
                                    columns = 'Gender',
                                    values=indicator,
                                    aggfunc = np.sum,
                                    margins=True)
        
    # regional percentages
    df_community['n_perc'] = df_community['n']/3000
    df_hoh['n_perc'] = df_hoh['n']/3000
    
    
    # expected values per disaggs
    
    for i in ['Refugee','Host - Rural','Host - Urban']:
        df_community[i+'_expected'] = df_community.loc[['All'],indicator+'_'+i]*df_community['n_perc']
        
    for i in ['Male','Female']:
        df_hoh[i+'_expected'] = df_hoh.loc[['All'],indicator+'_'+i]*df_hoh['n_perc'] 
        
    # individual chi-squareds    
    df_community[i+'_c2'] = (df_community[i+'_expected'] - df_community[indicator+'_'+i])**2/df_community[i+'_expected']
    df_hoh[i+'_c2'] = (df_hoh[i+'_expected'] - df_hoh[indicator+'_'+i])**2/df_hoh[i+'_expected']

    # total chi-squared
    c2_community = df_community.loc[['All'],'Refugee_c2'] + df_community.loc[['All'],'Host - Rural_c2'] + df_community.loc[['All'],'Host - Urban_c2']
   
    c2_hoh = df_hoh.loc[['All'],'Male_c2'] + df_hoh.loc[['All'],indicator+'_'+'Female_c2']
    
    
    # Significance cals
    
    from scipy.stats import chisqprob
    
    # degrees of freedom
    dof_community = len(df['Region'].valuecounts()-1)*2
    dof_hoh = len(df['Region'].valuecounts())-1           
                  
    # chi-squared_probability
                  
    c2p_community = chisqprob(3.84, dof_community)
    c2p_hoh = chisqprob(3.84, dof_hoh)
    
    return({'community_p': c2p_community,'hoh_p' : c2p_hoh})   

def chi_square_nested(df,indicator):
    
    # create pivots for disaggs
    df2 = df
    df2.loc[df[indicator] == -777, indicator] = 0
    dftemp = pd.pivot_table(df2,index='Region',
                                    columns = 'Community',
                                    values=indicator,
                                    aggfunc = np.sum,
                                    margins=True)
    
    df2 = df
    df2.loc[df[indicator] == -777, indicator] = 0
    dftemp = pd.pivot_table(df2,index='Region',
                                    columns = 'Gender',
                                    values=indicator,
                                    aggfunc = np.sum,
                                    margins=True)
        
    # regional percentages
    df_community['n_perc'] = df_community['n']/3000
    df_hoh['n_perc'] = df_hoh['n']/3000
    
    
    # expected values per disaggs
    
    for i in ['Refugee','Host - Rural','Host - Urban']:
        df_community[i+'_expected'] = df_community.loc[['All'],indicator+'_'+i]*df_community['n_perc']
        
    for i in ['Male','Female']:
        df_hoh[i+'_expected'] = df_hoh.loc[['All'],indicator+'_'+i]*df_hoh['n_perc'] 
        
    # individual chi-squareds    
    df_community[i+'_c2'] = (df_community[i+'_expected'] - df_community[indicator+'_'+i])**2/df_community[i+'_expected']
    df_hoh[i+'_c2'] = (df_hoh[i+'_expected'] - df_hoh[indicator+'_'+i])**2/df_hoh[i+'_expected']

    # total chi-squared
    c2_community = df_community.loc[['All'],'Refugee_c2'] + df_community.loc[['All'],'Host - Rural_c2'] + df_community.loc[['All'],'Host - Urban_c2']
   
    c2_hoh = df_hoh.loc[['All'],'Male_c2'] + df_hoh.loc[['All'],indicator+'_'+'Female_c2']
    
    
    # Significance cals
    
    from scipy.stats import chisqprob
    
    # degrees of freedom
    dof_community = len(df['Region'].valuecounts()-1)*2
    dof_hoh = len(df['Region'].valuecounts())-1           
                  
    # chi-squared_probability
                  
    c2p_community = chisqprob(3.84, dof_community)
    c2p_hoh = chisqprob(3.84, dof_hoh)
    
    return({'community_p': c2p_community,'hoh_p' : c2p_hoh}) 
    
                  
                     
def anova(df,indicator):
    return(0)              
                  
                  
def t_test(df,indicator):
    dftemp = pd.pivot_table(df,index=region,
                                    aggfunc=[np.count_nonzero,min,np.median,max,np.mean],
                                    values = indicator,
                                    columns = 'Gender',
                                    margins=True)
    
    dftemp = dftemp.rename(columns={'count_nonzero': 'n', 'min': 'Min','median': 'Med','max':'Max','mean':'Avg'})

def p_rc_nested_table(df,indicator):
    dftemp = pd.pivot_table(df,index=['Region','Community'],
                                    columns= indicator,
                                    values = 'n',
                                    aggfunc = np.sum,
                                    margins=True)
    
    camprows = []
    urbanrows = []
    ruralrows = []
    for region in ['Afar', 'Benishangul-Gumuz', 'Gambella', 'Somali', 'Tigray']:
        camprows.append((region,'Camp'))
        urbanrows.append((region,'Urban'))
        ruralrows.append((region,'Rural'))
    
    dftemp.loc[('Total','Camp'),:] = dftemp.loc[camprows,:].sum(axis=0).values
    dftemp.loc[('Total','Urban'),:] = dftemp.loc[urbanrows,:].sum(axis=0).values
    dftemp.loc[('Total','Rural'),:] = dftemp.loc[ruralrows,:].sum(axis=0).values
    
    
    cols = list(dftemp)
    dftemp['n'] = dftemp['All']
    dftemp['n'] = dftemp['All']
    values = dftemp.loc['All',:].values

    for j in list(dftemp):
        allj = dftemp.loc['All','All']
        dftemp[j] = dftemp[j].apply(lambda x: x/allj)

    dftemp.loc[:,cols] = dftemp.loc[:,cols].applymap("{0:.1%}".format)

    dftemp = dftemp.rename(index={'All': 'Total'})
    dftemp = dftemp.rename(columns={'All': 'Total'})

    cols = list(dftemp)
    cols.remove('n')
    reorder = ['n'] +cols
    dftemp.loc['n',:] = values
    dftemp = dftemp[reorder]


    dftemp = dftemp.reindex(['n','Afar', 'Benishangul-Gumuz', 'Gambella', 'Somali', 'Tigray', 'Total'],level=0)
    dftemp = dftemp.reindex(['Camp','Rural','Urban',''],level=1)

    return(dftemp)
    
def p_rc_table(df,indicator):
    df2 = df
    df2.loc[df[indicator] == -777, indicator] = 0
    dftemp = pd.pivot_table(df2,index='Region',
                                    columns = 'Community',
                                    values=indicator,
                                    aggfunc = np.sum,
                                    margins=True)
    
    dftemp['n'] = dftemp['All']
    values = dftemp.loc['All',:].values
    
    for j in ['Camp','Urban','Rural','All']:
        allj = dftemp.loc[['All'],'All']  
        dftemp[j] = dftemp[j].apply(lambda x: x/allj)

    cols = ['Camp','Urban','Rural','All']
    dftemp[cols] = dftemp[cols].applymap("{0:.0%}".format)
    
    dftemp.loc['n',:] = values
    
    cols = list(dftemp)
    cols.remove('n')
    reorder = ['n'] +cols
    dftemp = dftemp[reorder]
    dftemp = dftemp.reindex(['n','Afar', 'Benishangul-Gumuz', 'Gambella', 'Somali', 'Tigray', 'All'])
    
    dftemp = dftemp.rename(index={'All': 'Total'})
    dftemp = dftemp.rename(columns={'All': 'Total'})
    
    
    
    return(dftemp)
                  


def p_rh_table(df,indicator):
    df2 = df
    df2.loc[df[indicator] == -777, indicator] = 0
    dftemp = pd.pivot_table(df2,index='Region',
                                    columns = 'Gender',
                                    values=indicator,
                                    aggfunc = np.sum,
                                    margins=True)
    
    dftemp['n'] = dftemp['All']
    values = dftemp.loc['All',:].values
    
    for j in ['Not Head of Household','Male','Female','All']:
        allj = dftemp.loc[['All'],'All']  
        dftemp[j] = dftemp[j].apply(lambda x: x/allj)

    cols = ['Not Head of Household','Male','Female','All']
    dftemp[cols] = dftemp[cols].applymap("{0:.0%}".format)
    
    dftemp.loc['n',:] = values
    
    cols = list(dftemp)
    cols.remove('n')
    reorder = ['n'] +cols
    dftemp = dftemp[reorder]
    dftemp = dftemp.reindex(['n','Afar', 'Benishangul-Gumuz', 'Gambella', 'Somali', 'Tigray', 'All'])
    
    dftemp = dftemp.rename(index={'All': 'Total'})
    dftemp = dftemp.rename(columns={'All': 'Total'})
    
    return(dftemp)


def d_rc_table(df,indicator):
    dftemp = pd.pivot_table(df,index='Region',
                                    aggfunc=[np.count_nonzero,min,np.median,max,np.mean],
                                    values = indicator,
                                    columns = 'Community',
                                    margins=True)
    
    dftemp = dftemp.rename(columns={'count_nonzero': 'n', 'min': 'Min','median': 'Med','max':'Max','mean':'Avg'})
    dftemp = dftemp.dropna()
    
    return(dftemp)

def d_rc_nested_table(df,indicator):
    dftemp = pd.pivot_table(df,index=['Region','Community'],
                                   # columns= indicator,
                                    values = indicator,
                                    aggfunc = [np.count_nonzero,min,np.median,max,np.mean],
                                    margins=False)

    dftemp = dftemp.rename(columns={'count_nonzero': 'n', 'min': 'Min','median': 'Med','max':'Max','mean':'Avg'})
    dftemp = dftemp.dropna()
    
    return(dftemp)

def d_rh_table(df,indicator):
    dftemp = pd.pivot_table(df,index='Region',
                                    aggfunc=[np.count_nonzero,min,np.median,max,np.mean],
                                    values = indicator,
                                    columns = 'Gender',
                                    margins=True)
    
    dftemp = dftemp.rename(columns={'count_nonzero': 'n', 'min': 'Min','median': 'Med','max':'Max','mean':'Avg'})
    dftemp = dftemp.dropna()
    
    return(dftemp)